# Watches API
This is a naive implementation of a checkout API which computes the final price
given a list of watch IDs while also applying a set of given discounts.

## Problem Statement
We would like you to build a simplified e-commerce API with a single endpoint that performs a
checkout action. The single endpoint should take a list of watches and return the total cost.

For example, given the following watches catalog:

| Watch ID | Watch Name | Unit Price | Discount |
|----------|------------|------------|----------|
| 001 | Rolex | 100 | 3 for 200 |
| 002 | Michael Kors | 80 | 2 for 100 |
| 003 | Swatch | 50 |   |
| 004 | Casio | 60 |   |

Note that the first two have an applicable discount, there are a few requirements
worth noting:

* There is no limit to the number of items or combinations of watches a user can checkout.
* There is no limit to the number of times a discount can be used.
* Similarly, a user can checkout a single item if they wish.

## Endpoint Reference
### Request
POST http://localhost:8000/checkout
### Headers
* Accept: `application/json`
* Content-Type: `application/json`
### Body
```json
[
"001",
"002",
"001",
"004",
"003"
]
```

### Response
### Headers
* Content-Type: `application/json`
### Body
```json
{"price": 360}
```

## Solution Overview
The solution was implemented as a single endpoint web application where topics
like Security were left out of the scope for the sake of simplicity in the demo.

### Tools used
The tools chosen to implement this solution were:

#### Python
Python is a high level programming language which is among the most popular languages
according to the most recent [TIOBE index](https://www.tiobe.com/tiobe-index/),
it took down C as the most popular programming language in Aug, 2022.

This is a language I feel very comfortable with as I have a few years of experience
with it and I enjoy writing code in Python. It has a very extense standard library
and core modules that can be used to simplify the code we write and make out lives
as Software Engineers easier while programming.

#### Django
Django is the web framework for perfectionists with deadlines, it's a full stack
very opinionated web application framework which provides all the needed scaffolding
to build on top of. It includes an ORM to interact with databases and mapping result
sets to domain model objects automatically, a template engine (not used in this
solution) to render HTML, routing, authentication, user management and generates
an admin site out of the box from the created model classes.

Chosing django as my framework made it easier for me to focus on the core functionality
without worrying on whether the connection to the DB happened or not or how to
route the requests to the corresponding controller on the backend.

Django is also a framewith I've worked with a lot and my main tool when I want to
prototype a proof of concept quickly because of all the functionalities it gives
out of the box.

#### Other tools
* PostgreSQL: the most reliable and robust Open Source ORDBMS out there in the
market. This is my choice most of the time for data storage.

* Docker Compose: a container orchestration framework ideal to spin up multiple
containers and run multi-container applications as if they were running in different
servers.

* Docker: a containarization tool to build isolated containers from images, this
makes the local development and testing closer to how things would be on a production
or staging environment.

### General architecture
Django comes with its own way of structuring projects, each module can be trated
as an app, it is a general rule of thumb not to have circular dependencies
between apps because it can cause breakages when the interpreter is generating
the byte code.

In a nutshell, I wrote four modules:

* Common: where all the common functionality lives, these are:
    * `TimestampAbstractModel`: I like to keep track of things, this is to add
    a `created_at` and an `updated_at` fields to each model to know when each instance was 
    created and when it was last changed
    * `ContentTypeMiddleware`: this enforces that all the requests come with the
    `Accept` and `Content-Type` headers set to any of the supported types

* Catalog: This is where the `Watch` model lives, since we have only one type
of merchandise because this is a Watch store, we only have a `Watch` class
which has the basic attributes like `price`, `brand` and `name`, it also has a
dependency on `promotions.models.FixedQuantityDiscount` which we will discuss next

* Promotions: This is where all the logic related to discounts should live. We only
have one type of discounts which is based on the quantity and gives a fixed price
for a given quantity of a product, this is implemented in the `FixedQuantityDiscount` 

* Payments: This module implements the payment flow which consists only of one
endpoint: `/checkout`, this computes the total price for a given list of non-unique
watches and implements the API described above.

### Test data
| Watch ID | Watch name | Price | Discount |
|----------|------------|-------|----------|
| 001 | Seiko 5 | 100 | 3 for 200 |
| 002 | Seiko Brian May RS | 500 |  |
| 003 | Timex Weekender | 50 | 2 for 60 |
| 004 | Orient Sun and Moon | 90 | 4 for 300 |
| 005 | Grand Seiko Spring Drive | 1000 |  |
| 006 | Tag Heuer Carrera | 10000 |  |
| 007 | Casio G Shock | 50 |  |

### Sample requests

```bash
curl -XPOST \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    localhost:8000/checkout \
    --data '["001", "002", "001", "001", "004"]'
```

Which should return:

```json
{"price": 790}
```

## Running and testing

There are two ways you can test some requests

### Test server
I've deployed an instance to my personal server, you can access it here:

* http://dv.iffm.me/admin
    * Username: admin
    * Password: dvrocks!
* http://dv.iffm.me/checkout

### Locally
You'll need to have [docker](https://docs.docker.com/engine/install/) and
[docker-compose](https://docs.docker.com/compose/install/) installed in your machine

Once installed, go to the root directory of the project and execute the following
on the terminal:

```
docker-compose up -d
```

This will run both postgres and the app on two separate containers placed into
the same docker network, then, you'll need to run the database migrations to
create the tables and insert the test data:

```
docker container exec watches_api-web-1 python manage.py migrate
docker container exec watches_api-web-1 python manage.py loaddata promotions/fixtures/promotions_data.json
docker container exec watches_api-web-1 python manage.py loaddata promotions/fixtures/catalog_data.json
```

Finally, run collectstatic to serve the admin site static files correctly and create
the superuser so that you can log into the admin site

```
docker container exec watches_api-web-1 python manage.py collectstatic
docker container exec -it watches_api-web-1 python manage.py createsuperuser
```

For running the test cases you can also do so inside the corresponding container:

```
docker container exec watches_api-web-1 python manage.py test
```

## Final words
If you reached here, thank you very much for taking the time to review this exercise
I had a good time building it and rediscovering Python and Django after a few years
of using other languages and frameworks.

