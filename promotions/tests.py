from django.test import TestCase

from .models import FixedQuantityDiscount


class TestFixedQuantityDiscountApply(TestCase):
    def setUp(self):
        self.item_price = 10
        self.discount_obj = FixedQuantityDiscount(
            name='Test Discount 3 for 20',
            quantity=3,
            fixed_amount=20.0,
        )

    def test_discount_ok(self):
        result = self.discount_obj.apply(
            qty=self.discount_obj.quantity,
            price=self.item_price,
        )
        self.assertEquals(self.discount_obj.fixed_amount, result)

    def test_discount_one_item(self):
        result = self.discount_obj.apply(
            qty=1,
            price=self.item_price,
        )
        self.assertEquals(self.item_price, result)

    def test_discount_two_items(self):
        result = self.discount_obj.apply(
            qty=2,
            price=self.item_price,
        )
        self.assertEquals(self.item_price * 2, result)

    def test_discount_twice(self):
        result = self.discount_obj.apply(
            qty=self.discount_obj.quantity * 2,
            price=self.item_price,
        )
        self.assertEquals(self.discount_obj.fixed_amount * 2, result)
    
    def test_discount_overflow(self):
        result = self.discount_obj.apply(
            qty=self.discount_obj.quantity * 2 + 2,
            price=self.item_price,
        )
        expected = (self.discount_obj.fixed_amount * 2) + (self.item_price * 2)
        self.assertEquals(expected, result)

