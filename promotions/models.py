import math

from django.db import models
from common.models import TimestampAbstractModel

# Create your models here.
class FixedQuantityDiscount(TimestampAbstractModel):
    name = models.CharField(max_length=30)
    quantity = models.IntegerField()
    fixed_amount = models.FloatField()

    def __str__(self):
        return f'{self.name}'


    def apply(self, qty: int, price: float) -> float:
        discounts = math.floor(qty / self.quantity)
        not_discounted = qty % self.quantity
        
        promotion_amount = discounts * self.fixed_amount
        normal_amount = not_discounted * price
        return promotion_amount + normal_amount

