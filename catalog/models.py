from django.db import models
from common.models import TimestampAbstractModel
from promotions.models import FixedQuantityDiscount


class Watch(TimestampAbstractModel):
    internal_id = models.CharField(max_length=3, unique=True)
    brand = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    price = models.FloatField()
    discount = models.ForeignKey(
        FixedQuantityDiscount,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f'{self.brand} - {self.model}'

