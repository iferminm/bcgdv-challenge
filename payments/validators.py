import json
from json.decoder import JSONDecodeError


class JsonRequestValidator:
    def __init__(self, request):
        self.request = request

    def is_valid(self):
        try:
            json_data = json.loads(self.request.body)
        except JSONDecodeError:
            return False
        return True
