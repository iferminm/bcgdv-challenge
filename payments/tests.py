import json
from http import HTTPStatus

from django.test import TestCase
from django.test import Client

from catalog.models import Watch
from promotions.models import FixedQuantityDiscount


class TestCheckoutEndpoint(TestCase):
    def setUp(self):
        self.discounted = '001'
        self.discounted_price = 10.0
        self.not_discounted = '002'
        self.not_discounted_price = 15.0

        FixedQuantityDiscount.objects.create(
            name='3 for 20',
            quantity=3,
            fixed_amount=20,
        )

        Watch.objects.create(
            internal_id=self.discounted,
            brand='watch 1',
            model='discounted',
            price=self.discounted_price,
            discount=FixedQuantityDiscount.objects.get(),
        )
        Watch.objects.create(
            internal_id=self.not_discounted,
            brand='watch 2',
            model='not discounted',
            price=self.not_discounted_price,
        )

        self.client = Client()
        self.json_type = 'application/json'

    def test_bad_headers_missing_accept(self):
        payload = ['001',]
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
        )
        self.assertEquals(HTTPStatus.UNSUPPORTED_MEDIA_TYPE, response.status_code)

    def test_bad_payload(self):
        payload = 'not a json'
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
            HTTP_ACCEPT=self.json_type,
        )
        self.assertEquals(HTTPStatus.BAD_REQUEST, response.status_code)

    def test_single_discount(self):
        payload = ['001', '001', '001']
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
            HTTP_ACCEPT=self.json_type,
        )
        self.assertEquals(HTTPStatus.OK, response.status_code)

        discount = FixedQuantityDiscount.objects.get()
        self.assertEquals({'price': discount.fixed_amount}, response.json())

    def test_discount_and_not_discounted(self):
        payload = ['001', '001', '001', '002']
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
            HTTP_ACCEPT=self.json_type,
        )
        self.assertEquals(HTTPStatus.OK, response.status_code)

        discount = FixedQuantityDiscount.objects.get()
        expected = discount.fixed_amount + self.not_discounted_price
        self.assertEquals({'price': expected}, response.json())

    def test_double_discount(self):
        payload = ['001', '001', '001', '001', '001', '001']
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
            HTTP_ACCEPT=self.json_type,
        )
        self.assertEquals(HTTPStatus.OK, response.status_code)

        discount = FixedQuantityDiscount.objects.get()
        self.assertEquals({'price': discount.fixed_amount * 2}, response.json())

    def test_discount_and_more_discounted(self):
        payload = ['001', '001', '001', '001' ]
        response = self.client.post(
            '/checkout',
            payload,
            content_type=self.json_type,
            HTTP_ACCEPT=self.json_type,
        )
        self.assertEquals(HTTPStatus.OK, response.status_code)

        discount = FixedQuantityDiscount.objects.get()
        expected = discount.fixed_amount + self.discounted_price
        self.assertEquals({'price': expected}, response.json())
