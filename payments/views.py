import json
from collections import Counter
from http import HTTPStatus

from django.http import JsonResponse
from django.views import View

from .validators import JsonRequestValidator
from catalog.models import Watch


class CheckoutView(View):
    def post(self, request):
        validator = JsonRequestValidator(request)
        if validator.is_valid():
            request_json = json.loads(request.body)
            response_content = self._process(request_json)
            status_code = HTTPStatus.OK
        else:
            response_content = {
                'error': 'The data must be a valid json'
            }
            status_code = HTTPStatus.BAD_REQUEST

        return JsonResponse(
            response_content,
            status=status_code,
        )

    def _process(self, watches_list) -> dict:
        watch_count = dict(Counter(watches_list))
        watches = Watch.objects.select_related('discount').filter(
            internal_id__in=watch_count.keys(),
        )

        purchase_total = 0
        for watch in watches:
            if watch.discount:
                current = watch.discount.apply(
                    qty=watch_count[watch.internal_id],
                    price=watch.price,
                )
            else:
                current = watch.price * watch_count[watch.internal_id]
            purchase_total += current

        return {'price': purchase_total}

