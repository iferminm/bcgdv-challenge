from http import HTTPStatus

from django.http import JsonResponse


SUPPORTED_CONTENT_TYPES = ('application/json',)
HEADERS = {'content-type': 'application/json'}
class ContentTypeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.startswith('/admin') or request.path in ("", "/"):
            return self.get_response(request)
        if not self._supports_content_types(request):
            payload = {
                'error': 'Unsupported content-type.',
                'supported': SUPPORTED_CONTENT_TYPES,
            }
            return JsonResponse(
                payload,
                status=HTTPStatus.UNSUPPORTED_MEDIA_TYPE,
            )
        
        return self.get_response(request)

    def _supports_content_types(self, request):
        accept_content_type = request.META.get('HTTP_ACCEPT')
        request_content_type = request.META.get('CONTENT_TYPE')
        supports_accept = accept_content_type in SUPPORTED_CONTENT_TYPES
        supports_request = request_content_type in SUPPORTED_CONTENT_TYPES
        return supports_accept and supports_request
